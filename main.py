import PySimpleGUI as sg
sg.theme('SandyBeach')
layout = [[sg.Text('Choose the type of property', size=(30, 1), font='Lucida', justification='left')],
          [sg.Combo(['Apartment', 'House', 'Room', 'Plot', 'Commercial premises', 'Warehouse', 'Garage'],
                    default_value='Apartment', key='board')],
          [sg.Text('Choose State ', size=(30, 1), font='Lucida', justification='left')],
          [sg.Combo(
              ['Alabama', 'Alaska''Arizona', 'Arkansas', 'Connecticut', 'South Dakota', 'North Dakota', 'Delaware',
               'DDistrict of Columbia', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa',
               'California', 'Kansas', 'South Carolina', 'North Carolina', 'Kentucky', 'Colorado', 'Louisiana', 'Maine',
               'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Missisipi', 'Missouri', 'Montana', 'Nebraska',
               'Nevada', 'New Hampshire', 'New Jersey', 'New York', 'New Mexico', 'Ohio', 'Oklahoma', 'Oregon',
               'Pennsylvania', 'Rhode Island', 'Texas', 'Tennessee', 'Utah', 'Vermont', 'Washington', 'Virginia',
               'West Virginia', 'Wisconsin', 'Wyoming'], key='dest')],
          [sg.Text('Choose a city', size=(30, 1), font='Lucida', justification='left')],
          [sg.Listbox(values=['Los Angles', 'San Francisco', 'San Diego', 'Sacramento', 'San Bruno'],
                      select_mode='extended', key='fac', size=(30, 6))],
          [sg.Button('PREDICT PRICE', font=('Times New Roman', 12)), sg.Button('CANCEL', font=('Times New Roman', 12))]]

win = sg.Window('Customise your property', layout, margins=(600, 300))
e, v = win.read()
win.close()
strx = ""
for val in v['fac']:
    strx = strx + " " + val + ","

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

housing = pd.read_csv('C:/Users/Speedy/Downloads/train.csv')

housing.head()

housing.shape

housing.info()

housing.Id

housing.hist(bins=50, figsize=(20, 15))
plt.show()

continuous = ['LotFrontage',
              'LotArea',
              'MasVnrArea',
              'BsmtFinSF1',
              'BsmtFinSF2',
              'BsmtUnfSF',
              'TotalBsmtSF',
              '1stFlrSF',
              '2ndFlrSF',
              'LowQualFinSF',
              'GrLivArea',
              'GarageArea',
              'WoodDeckSF',
              'OpenPorchSF',
              'EnclosedPorch',
              '3SsnPorch',
              'ScreenPorch',
              'PoolArea',
              'MiscVal',
              'SalePrice']

housing[continuous].hist(bins=50, figsize=(20, 15))
plt.show()

housing['PoolArea'].value_counts()

housing['LowQualFinSF'].value_counts()

housing.describe()

housing.select_dtypes(include=[object]).columns.tolist()

categorical = ['MSSubClass',
               'MSZoning',
               'Street',
               'Alley',
               'LotShape',
               'LandContour',
               'Utilities',
               'LotConfig',
               'LandSlope',
               'Neighborhood',
               'Condition1',
               'Condition2',
               'BldgType',
               'HouseStyle',
               'OverallQual',
               'OverallCond',
               'RoofStyle',
               'RoofMatl',
               'Exterior1st',
               'Exterior2nd',
               'MasVnrType',
               'ExterQual',
               'ExterCond',
               'Foundation',
               'BsmtQual',
               'BsmtCond',
               'BsmtExposure',
               'BsmtFinType1',
               'BsmtFinType2',
               'Heating',
               'HeatingQC',
               'CentralAir',
               'Electrical',
               'KitchenQual',
               'Functional',
               'FireplaceQu',
               'GarageType',
               'GarageFinish',
               'GarageQual',
               'GarageCond',
               'PavedDrive',
               'PoolQC',
               'Fence',
               'MiscFeature',
               'SaleType',
               'SaleCondition']

housing['PoolQC']

housing.columns[housing.isnull().any()].size

missing_cols = housing.columns[housing.isnull().any()].tolist()
missing_cols

missing = 1 - housing.count() / len(housing)
missing

missing = missing[missing > 0]
missing

missing.sort_values(ascending=False)

missing.sort_values().plot.bar(color=np.random.rand(len(missing), 3))
plt.title('Features with Missing Values')

housing[missing_cols]

housing1 = housing[missing_cols].copy()
housing1[missing_cols] = np.where(housing[missing[missing > 0].index].isnull(), 1, 0)
housing1 = pd.concat([housing1, housing[['SalePrice']]], axis=1)
housing1

for feature in missing_cols:
    housing1.groupby(feature)['SalePrice'].median().plot.bar()


numerical = housing.select_dtypes(include=[np.number]).columns.tolist()
numerical

correlation = housing[numerical].corr()
correlation

correlation['SalePrice'].sort_values(ascending=False)

f, ax = plt.subplots(figsize=(25, 25))
sns.heatmap(correlation, vmax=.8, linewidths=0.01, linecolor='white', square=True)

n = 11
features = correlation['SalePrice'].sort_values(ascending=False).index.tolist()[:n]
features

np.corrcoef(housing[features].values)

correlation.loc[features, features]

f, ax = plt.subplots(figsize=(10, 10))
sns.heatmap(correlation.loc[features, features], vmax=0.8, annot=True, linewidths=0.01, linecolor='white', square=True)
plt.title('Correlation Among Select Numerical Features')

plt.scatter(housing['GrLivArea'], housing['SalePrice'])


housing['GrLivArea'].nlargest(5)

f, ax = plt.subplots(figsize=(10, 5))


for feature in continuous:
    f, ax = plt.subplots(figsize=(12, 3))
    sns.boxplot(x=housing[feature])

train, test = pd.read_csv('C:/Users/Speedy/Downloads/train.csv'), pd.read_csv('C:/Users/Speedy/Downloads/test.csv')

dataset = pd.concat((train.loc[:, 'MSSubClass':], test.loc[:, 'MSSubClass':]))
dataset.shape

def handle_outliers(dataset):
    data = dataset.copy()
    condition1 = data['GrLivArea'] < 4000
    condition2 = (data['LotFrontage'].isna()) | (data['LotFrontage'] < 300)
    condition3 = data['LotArea'] < 100000
    return data[condition1 & condition2 & condition3]

dataset = handle_outliers(dataset)

def replace_na_cat(dataset, features):
    data = dataset.copy()
    data[features] = data[features].fillna('NotApplicable')
    return data


features = ['Alley',
            'MasVnrType',
            'BsmtQual',
            'BsmtCond',
            'BsmtExposure',
            'BsmtFinType1',
            'BsmtFinType2',
            'FireplaceQu',
            'GarageType',
            'GarageFinish',
            'GarageQual',
            'GarageCond',
            'PoolQC',
            'Fence',
            'MiscFeature']

dataset = replace_na_cat(dataset, features)

def replace_na_num(dataset, features):
    data = dataset.copy()
    data[features] = data[features].fillna(0)
    return data


features = ['MasVnrArea', 'GarageYrBlt', 'LotFrontage']

dataset = replace_na_num(dataset, features)

def remove_missing(dataset, features):
    data = dataset.copy()
    return data[~data[features].isna()]


features = 'Electrical'

dataset = remove_missing(dataset, features)

object_features = dataset.select_dtypes(include=[np.object]).columns
object_features

nominal = ['MSSubClass',
           'MSZoning',
           'Street',
           'Alley',
           'LandContour',
           'LotConfig',
           'Neighborhood',
           'Condition1',
           'Condition2',
           'BldgType',
           'HouseStyle',
           'RoofStyle',
           'RoofMatl',
           'Exterior1st',
           'Exterior2nd',
           'MasVnrType',
           'Foundation',
           'Heating',
           'CentralAir',
           'GarageType',
           'MiscFeature',
           'SaleType',
           'SaleCondition']

ordinal = ['LotShape',
           'Utilities',
           'LandSlope',
           'OverallQual',
           'OverallCond',
           'ExterQual',
           'ExterCond',
           'BsmtQual',
           'BsmtCond',
           'BsmtExposure',
           'BsmtFinType1',
           'BsmtFinType2',
           'HeatingQC',
           'Electrical',
           'KitchenQual',
           'Functional',
           'FireplaceQu',
           'GarageFinish',
           'GarageQual',
           'GarageCond',
           'PavedDrive',
           'PoolQC',
           'Fence']

noms = set(object_features) & set(nominal)
noms

ords = set(object_features) & set(ordinal)
ords


def encode_noms(dataset, features):
    import pandas as pd
    data = dataset.copy()
    return pd.get_dummies(data, columns=features, drop_first=True)


dataset = encode_noms(dataset, noms)


def encode_ords(dataset, features):
    #     pip install category_encoders
    from category_encoders.ordinal import OrdinalEncoder
    data = dataset.copy()
    enc = OrdinalEncoder(cols=features, return_df=True)
    return enc.fit_transform(data)

dataset = encode_noms(dataset, ords)

from sklearn.model_selection import GridSearchCV, train_test_split
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb


dataset.rename(columns=lambda col: col.replace('.', ''), inplace=True)

X = dataset[:145].drop(columns=['SalePrice'])

x = dataset[145:].drop(columns=['SalePrice'])

Y = dataset['SalePrice'][:145]

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, random_state=42, shuffle=True)

params1 = {
    'n_estimators': [50],
    'max_depth': [3, 5],
    'learning_rate': np.linspace(0.001, 0.1, 4),  #
    'gamma': np.linspace(0, 1, 5),  #
    'min_child_weight': np.linspace(1, 5, 3),  #
    'subsample': np.linspace(0.3, 0.9, 3),  #
    'colsample_bytree': np.linspace(0.3, 0.9, 3),  #
    'reg_alpha': [0, .01, .1, .5, 1],  #
    'reg_lambda': [0, .01, .1, .5, 1],  #
    'scale_pos_weight': np.linspace(1, 5, 3)  #
}


xgb_estimator1 = xgb.XGBRegressor(seed=42)



gsearch1 = GridSearchCV(estimator=xgb_estimator1, param_grid=params1,
                        scoring='neg_root_mean_squared_error', n_jobs=-1,
                        refit='neg_root_mean_squared_error', cv=5, verbose=11)

gsearch1.fit(X_train, y_train)


print(list(gsearch1.cv_results_.keys()))
gsearch1.cv_results_

print(f'Best Estimator: {gsearch1.best_estimator_}')
print(f'Best Parameters: {gsearch1.best_params_}')
print(f'Best Score: {gsearch1.best_score_}')

xgb.plot_tree(gsearch1.best_estimator_)
plt.rcParams['figure.figsize'] = [50, 10]
plt.show()

xgb.plot_importance(gsearch1.best_estimator_)
plt.rcParams['figure.figsize'] = [100, 20]
plt.show()

xgb_best1 = gsearch1.best_estimator_

plt.rcParams['figure.figsize'] = [200, 200]
plt.show()

print(xgb_best1.fit(X_train, y_train))
print(xgb_best1.score(X_test, y_test))

xgb_best1.fit(X, Y)


from explainerdashboard import RegressionExplainer, ExplainerDashboard

feature_descriptions = {
    'MSSubClass': 'Identifies the type of dwelling involved in the sale',
    'MSZoning': 'Identifies the general zoning classification of the sale',
    'LotFrontage': 'Linear feet of street connected to property',
    'LotArea': 'Lot size in square feet',
    'Street_Pave': 'Type of road access to property',
    'Alley': 'Type of alley access to property',
    'LotShape': 'General shape of property',
    'LandContour': 'Flatness of the property',
    'Utilities_NoSeWa': 'Type of utilities available',
    'LotConfig': 'Lot configuration',
    'LandSlope': 'Slope of property',
    'Neighborhood': 'Physical locations within Ames city limits',
    'Condition1': 'Proximity to various conditions',
    'Condition2': 'Proximity to various conditions ',
    'BldgType': 'Type of dwelling',
    'HouseStyle': 'Style of dwelling',
    'OverallQual': 'Rates the overall material and finish of the house',
    'OverallCond': 'Rates the overall condition of the house',
    'YearBuilt': 'Original construction date',
    'YearRemodAdd': 'Remodel date ',
    'RoofStyle': 'Type of roof',
    'RoofMatl': 'Roof material',
    'Exterior1st': 'Exterior covering on house',
    'Exterior2nd': 'Exterior covering on house (if more than one material)',
    'MasVnrType': 'Masonry veneer type',
    'MasVnrArea': 'Masonry veneer area in square feet',
    'ExterQual': 'Evaluates the quality of the material on the exterior',
    'ExterCond': 'Evaluates the present condition of the material on the exterior',
    'Foundation': 'Type of foundation',
    'BsmtQual': 'Evaluates the height of the basement',
    'BsmtCond': 'Evaluates the general condition of the basement',
    'BsmtExposure': 'Refers to walkout or garden level walls',
    'BsmtFinType1': 'Rating of basement finished area',
    'BsmtFinSF1': 'Type 1 finished square feet',
    'BsmtFinType2': 'Rating of basement finished area',
    'BsmtFinSF2': 'Type 2 finished square feet',
    'BsmtUnfSF': 'Unfinished square feet of basement area',
    'TotalBsmtSF': 'Total square feet of basement area',
    'Heating': 'Type of heating',
    'HeatingQC': 'Heating quality and condition',
    'CentralAir_Y': 'Central air conditioning',
    'Electrical': 'Electrical system',
    '1stFlrSF': 'First Floor square feet',
    '2ndFlrSF': 'Second floor square feet',
    'LowQualFinSF': 'Low quality finished square feet ',
    'GrLivArea': 'Above grade (ground) living area square feet',
    'BsmtFullBath': 'Basement full bathrooms',
    'BsmtHalfBath': 'Basement half bathrooms',
    'FullBath': 'Full bathrooms above grade',
    'HalfBath': 'Half baths above grade',
    'Bedroom': 'Bedrooms above grade ',
    'Kitchen': 'Kitchens above grade',
    'KitchenQual': ' Kitchen quality',
    'TotRmsAbvGrd': 'Total rooms above grade ',
    'Functional': 'Home functionality ',
    'Fireplaces': 'Number of fireplaces',
    'FireplaceQu': 'Fireplace quality',
    'GarageType': 'Garage location',
    'GarageYrBlt': 'Year garage was built',
    'GarageFinish': 'Interior finish of the garage',
    'GarageCars': 'Size of garage in car capacity',
    'GarageArea': 'Size of garage in square feet',
    'GarageQual': 'Garage quality',
    'GarageCond': 'Garage condition',
    'PavedDrive': 'Paved driveway',
    'WoodDeckSF': 'Wood deck area in square feet',
    'OpenPorchSF': 'Open porch area in square feet',
    'EnclosedPorch': 'Enclosed porch area in square feet',
    '3SsnPorch': 'Three season porch area in square feet',
    'ScreenPorch': 'Screen porch area in square feet',
    'PoolArea': 'Pool area in square feet',
    'PoolQC': 'Pool quality',
    'Fence': 'Fence quality',
    'MiscFeature': 'Miscellaneous ',
    'MiscVal': 'Value of miscellaneous feature',
    'MoSold': 'Month Sold (MM)',
    'YrSold': 'Year Sold (YYYY)',
    'SaleType': 'Type of sale',
    'SaleCondition': 'Condition of sale',
    'SalePrice': 'Sale price'
}

cats = noms.union(ords) - set(('Utilities', 'Street', 'CentralAir'))
cats

REmodel = xgb_best1.fit(X, Y)
explainer = RegressionExplainer(REmodel, X, Y,
                                cats=list(cats),
                                descriptions=feature_descriptions,
                                units="$")
db = ExplainerDashboard(explainer,
                        title='Predicting House Prices',
                        mode='inline')
db.run()
